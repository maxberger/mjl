/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.test;

import java.util.List;
import java.util.Map;

import junit.framework.Assert;
import name.berger.max.json.JsonDeserializer;
import name.berger.max.json.parser.ParseException;

public class DeserializerTest {

    final JsonDeserializer jsonDeserializer = JsonDeserializer.getInstance();

    @org.testng.annotations.Test
    public void testSimpleTypes() throws Exception {
        Assert.assertEquals(Long.valueOf(7), jsonDeserializer.deserialize("7"));
        Assert.assertEquals(Double.valueOf(3.14), jsonDeserializer
                .deserialize("3.14"));
        Assert.assertEquals(Boolean.FALSE, jsonDeserializer
                .deserialize("false"));
        Assert.assertEquals(Boolean.TRUE, jsonDeserializer.deserialize("true"));
        Assert.assertNull(jsonDeserializer.deserialize("null"));
    }

    @SuppressWarnings("unchecked")
    @org.testng.annotations.Test
    public void testArrayTypes() throws Exception {
        List<Integer> l = (List<Integer>) jsonDeserializer.deserialize("[1,2]");
        Assert.assertEquals(2, l.size());
        Assert.assertEquals(Long.valueOf(1), l.get(0));
        Assert.assertEquals(Long.valueOf(2), l.get(1));
    }

    @SuppressWarnings("unchecked")
    @org.testng.annotations.Test
    public void testMap() throws Exception {
        Map<String, Object> testMap = (Map<String, Object>) jsonDeserializer
                .deserialize("{\"One\":1,\"Two\":\"ii\"}");
        Assert.assertEquals(2, testMap.size(), 2);
        Assert.assertEquals(Long.valueOf(1), testMap.get("One"));
        Assert.assertEquals("ii", testMap.get("Two"));
    }

    @org.testng.annotations.Test
    public void testString() throws Exception {
        Assert.assertEquals(jsonDeserializer.deserialize("\"bla\""), "bla");
        Assert.assertEquals(jsonDeserializer.deserialize("\"bl\\u00E4\""),
                "bl\u00e4");
    }

    @org.testng.annotations.Test
    public void testFailure() throws Exception {
        try {
            jsonDeserializer.deserialize("}{");
            Assert.fail("No failure on illegal object");
        } catch (ParseException pe) {
            // Good!
        }
    }

    @org.testng.annotations.Test
    public void testEscape() throws Exception {
        Assert.assertEquals(jsonDeserializer.deserialize("\"bl\\/a\""), "bl/a");
        try {
            jsonDeserializer.deserialize("\"bl\\\"");
            Assert.fail("No failure on \\ at the end of input!");
        } catch (ParseException pe) {
            // Good!
        }
    }
}
