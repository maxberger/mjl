/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.test;

import java.io.File;
import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import name.berger.max.json.JsonSerializer;

import org.testng.Assert;

public class SerializerTest {

	final JsonSerializer jsonSerializer = JsonSerializer.getInstance();

	@org.testng.annotations.Test
	public void testSimpleTypes() throws Exception {
		Assert.assertEquals(jsonSerializer.serialize((Integer) 7), "7");
		Assert.assertEquals(jsonSerializer.serialize((Float) 3.14f), "3.14");
		Assert.assertEquals(jsonSerializer.serialize((Double) 3.14), "3.14");
		Assert.assertEquals(jsonSerializer.serialize(Boolean.TRUE), "true");
		Assert.assertEquals(jsonSerializer.serialize(Boolean.FALSE), "false");
		Assert.assertEquals(jsonSerializer.serialize(null), "null");
	}

	@org.testng.annotations.Test
	public void testArrayTypes() throws Exception {
		List<Integer> l = new ArrayList<Integer>();
		l.add(1);
		l.add(2);
		Assert.assertEquals(jsonSerializer.serialize(l), "[1,2]");

		int[] ia = { 1, 2 };
		Assert.assertEquals(jsonSerializer.serialize(ia), "[1,2]");
	}

	@org.testng.annotations.Test
	public void testMap() throws Exception {
		Map<String, Object> testMap = new TreeMap<String, Object>();
		testMap.put("One", 1);
		testMap.put("Two", "ii");
		Assert.assertEquals(jsonSerializer.serialize(testMap),
				"{\"One\":1,\"Two\":\"ii\"}");
	}

	@org.testng.annotations.Test
	public void testString() throws Exception {
		Assert.assertEquals(jsonSerializer.serialize("bla"), "\"bla\"");
		Assert.assertEquals(jsonSerializer.serialize("bl\u00e4"),
				"\"bl\\u00E4\"");
	}

	@org.testng.annotations.Test
	public void testFailure() throws Exception {
		try {
			jsonSerializer.serialize(new File("/"));
			Assert.fail("No failure on unserializable object");
		} catch (NotSerializableException nse) {
			// Good!
		}
	}

}
