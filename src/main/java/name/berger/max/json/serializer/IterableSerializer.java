/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.serializer;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import name.berger.max.json.JsonSerializer;

/**
 * Serializer for iterable data types. This includes most Lists.
 * 
 * @version $Date$
 */
public class IterableSerializer implements Serializer {

    /**
     * Default constructor.
     */
    public IterableSerializer() {
        // Nothing to do.
    }

    /** {@inheritDoc} */
    public Collection<Class<?>> getSupportedClasses() {
        final List<Class<?>> ret = new ArrayList<Class<?>>(1);
        ret.add(Iterable.class);
        return ret;
    }

    /** {@inheritDoc} */
    public String serialize(final Object o, final JsonSerializer ser)
            throws NotSerializableException {
        final StringBuilder s = new StringBuilder();
        s.append('[');
        final Iterator<?> it = ((Iterable<?>) o).iterator();
        while (it.hasNext()) {
            s.append(ser.serialize(it.next()));
            if (it.hasNext()) {
                s.append(',');
            }
        }
        s.append(']');
        return s.toString();
    }

}
