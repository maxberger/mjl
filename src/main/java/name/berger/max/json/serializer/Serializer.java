/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.serializer;

import java.io.NotSerializableException;
import java.util.Collection;

import name.berger.max.json.JsonSerializer;

/**
 * Common interface for all serializers.
 * 
 * @version $Date$
 */
public interface Serializer {

    /**
     * Retrieves a list of Classes supported by this serializer.
     * 
     * @return List of supported classes.
     */
    Collection<Class<?>> getSupportedClasses();

    /**
     * Serialize the given object to a string. It may safely be assumed that
     * this method is only called with objects of a type given by
     * {@link #getSupportedClasses()}.
     * 
     * @param object
     *            Object to serialize
     * @param ser
     *            The calling serializer, if needed for to serialize embedded
     *            objects.
     * @return a String representing the JSon of the given object.
     * @throws NotSerializableException
     *             if the object cannot be serialized for any reason.
     */
    String serialize(Object object, JsonSerializer ser)
            throws NotSerializableException;
}
