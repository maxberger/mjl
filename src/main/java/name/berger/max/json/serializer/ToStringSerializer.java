/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.serializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import name.berger.max.json.JsonSerializer;

/**
 * Serializer which uses toString for serialization. Works for Boolean and
 * Number data types.
 * 
 * @version $Date$
 */
public class ToStringSerializer implements Serializer {

    /**
     * Default constructor.
     */
    public ToStringSerializer() {
        // Nothing to do.
    }

    /** {@inheritDoc} */
    public String serialize(final Object o, final JsonSerializer s) {
        return o.toString();
    }

    /** {@inheritDoc} */
    public Collection<Class<?>> getSupportedClasses() {
        final List<Class<?>> ret = new ArrayList<Class<?>>(2);
        ret.add(Number.class);
        ret.add(Boolean.class);
        return ret;
    }
}
