/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.serializer;

import java.io.NotSerializableException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import name.berger.max.json.JsonSerializer;

/**
 * Serializer for Strings. Will return only characters in the standard 7-bit
 * ASCII range.
 * 
 * @version $Date$
 */
public class StringSerializer implements Serializer {

    private static final int MAX_CHARS_FOR_U_LITERAL = 4;

    /**
     * Default constructor.
     */
    public StringSerializer() {
        // Nothing to do.
    }

    /** {@inheritDoc} */
    public Collection<Class<?>> getSupportedClasses() {
        final List<Class<?>> ret = new ArrayList<Class<?>>(3);
        ret.add(String.class);
        ret.add(URI.class);
        ret.add(URL.class);
        return ret;
    }

    /** {@inheritDoc} */
    public String serialize(final Object o, final JsonSerializer ser)
            throws NotSerializableException {
        final String str = o.toString();
        final StringBuilder s = new StringBuilder();
        s.append('"');
        for (final char c : str.toCharArray()) {
            if ((c >= '\u0020') && (c <= '\u007E')) {
                s.append(c);
            } else {
                s.append("\\u");
                final String hex = Integer.toHexString(c)
                        .toUpperCase(Locale.US);
                for (int i = hex.length(); i < StringSerializer.MAX_CHARS_FOR_U_LITERAL; i++) {
                    s.append('0');
                }
                s.append(hex);
            }
        }
        s.append('"');
        return s.toString();

    }

}
