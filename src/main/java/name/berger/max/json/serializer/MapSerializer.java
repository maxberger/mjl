/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json.serializer;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import name.berger.max.json.JsonSerializer;

/**
 * Serializer for Maps.
 * 
 * @version $Date$
 */
public class MapSerializer implements Serializer {

    /**
     * Default constructor.
     */
    public MapSerializer() {
        // Nothing to do.
    }

    /** {@inheritDoc} */
    public Collection<Class<?>> getSupportedClasses() {
        final List<Class<?>> ret = new ArrayList<Class<?>>(1);
        ret.add(Map.class);
        return ret;
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    public String serialize(final Object o, final JsonSerializer ser)
            throws NotSerializableException {
        final Map<String, Object> map = (Map<String, Object>) o;
        final StringBuilder s = new StringBuilder();
        s.append('{');
        boolean first = true;
        for (final Entry<String, Object> entry : map.entrySet()) {
            if (first) {
                first = false;
            } else {
                s.append(',');
            }
            s.append(ser.serialize(entry.getKey()));
            s.append(':');
            s.append(ser.serialize(entry.getValue()));

        }
        s.append('}');
        return s.toString();
    }

}
