/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json;

import java.io.NotSerializableException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import name.berger.max.json.serializer.ArraySerializer;
import name.berger.max.json.serializer.IterableSerializer;
import name.berger.max.json.serializer.MapSerializer;
import name.berger.max.json.serializer.Serializer;
import name.berger.max.json.serializer.StringSerializer;
import name.berger.max.json.serializer.ToStringSerializer;

/**
 * Create JSon from java data structures.
 * <p>
 * This class tries to interpret all standard Java data structures, such as
 * List, Map, Array, Boolean, String and Number. It uses the serializisers from
 * the {@link name.berger.max.json.serilizer} package.
 * <p>
 * This class is Thread safe.
 * 
 * @version $Date$
 */
public final class JsonSerializer {

    private final Map<Class<?>, Serializer> serializers = new HashMap<Class<?>, Serializer>();

    private JsonSerializer() {
        this.addSerializer(new ToStringSerializer());
        this.addSerializer(new IterableSerializer());
        this.addSerializer(new ArraySerializer());
        this.addSerializer(new MapSerializer());
        this.addSerializer(new StringSerializer());
    }

    private void addSerializer(final Serializer serializer) {
        for (final Class<?> c : serializer.getSupportedClasses()) {
            this.serializers.put(c, serializer);
        }
    }

    private Serializer findSerializer(final Class<?> cls) {
        Serializer found = this.serializers.get(cls);
        if ((found == null) && (cls.isArray())) {
            found = this.serializers.get(Array.class);
        }
        if (found == null) {
            final Class<?>[] interfaces = cls.getInterfaces();
            int i = 0;
            while ((found == null) && (i < interfaces.length)) {
                found = this.findSerializer(interfaces[i]);
                i++;
            }
        }
        if (found == null) {
            final Class<?> sup = cls.getSuperclass();
            if (sup != null) {
                found = this.findSerializer(sup);
            }
        }
        return found;
    }

    /**
     * Serialize Java Data objects into JSon.
     * <p>
     * Please note: Successful serialization is only supported for the given
     * data types. The Java Serializable interface has no effect.
     * 
     * @param rootObject
     *            Root object for the JSon to create.
     * @return A String representing the JSon. The String will only contain
     *         characters from the 7-bit ASCII range and can thefore be sent
     *         safely in most encodings.
     * @throws NotSerializableException
     *             If any of the data structures encapsulated in the original
     *             object could not be serialized.
     */
    public String serialize(final Object rootObject)
            throws NotSerializableException {
        if (rootObject == null) {
            return "null";
        } else {
            final Serializer ser = this.findSerializer(rootObject.getClass());
            if (ser == null) {
                throw new NotSerializableException(rootObject.getClass()
                        .toString());
            } else {
                return this.findSerializer(rootObject.getClass()).serialize(
                        rootObject, this);
            }
        }
    }

    /**
     * Retrieve an instance of the serializer.
     * 
     * @return An instance of the serializer.
     */
    public static JsonSerializer getInstance() {
        return new JsonSerializer();
    }

}
