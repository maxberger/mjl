/*
 * Copyright 2008 - 2009 by Max Berger
 * 
 * This file is part of MJL.
 * 
 * MJL is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * MJL is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser Public License along with
 * MJL. If not, see <http://www.gnu.org/licenses/>.
 */

package name.berger.max.json;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import name.berger.max.json.parser.JsonHandler;
import name.berger.max.json.parser.ParseException;

/**
 * Serialize JSon into Java classes.
 * <p>
 * Given a source in any format, this class will create the matching Java data
 * structures.
 * <p>
 * Use {@link #getInstance()} to create an instance of this class. Use any of
 * the provided classes to create the java data structures.
 * <p>
 * This class is Thread safe.
 * 
 * @version $Date$
 */
public final class JsonDeserializer {

    private JsonDeserializer() {
        // nothing to do.
    }

    /**
     * Retrieve an instance of the de-serializer.
     * 
     * @return An instance of the de-serializer.
     */
    public static JsonDeserializer getInstance() {
        return new JsonDeserializer();
    }

    /**
     * Deserialize a source given as InputStream. This method assumes UTF-8
     * encoding.
     * 
     * @param inputStream
     *            InputStream to parse
     * @return An instance of an object representing the top-level element.
     * @throws ParseException
     *             if a parse error occurs.
     */
    public Object deserialize(final InputStream inputStream) throws ParseException {
        final JsonHandler handler = new JsonHandler(inputStream, "UTF-8");
        return handler.object();
    }

    /**
     * Deserialize a source given as InputStream with given encoding.
     * 
     * @param inputStream
     *            InputStream to parse
     * @param encoding
     *            an encoding given in Java notation.
     * @return An instance of an object representing the top-level element.
     * @throws ParseException
     *             if a parse error occurs.
     */
    public Object deserialize(final InputStream inputStream, final String encoding)
            throws ParseException {
        final JsonHandler handler = new JsonHandler(inputStream, encoding);
        return handler.object();
    }

    /**
     * Deserialize a source given as Reader.
     * 
     * @param reader
     *            Reader for input.
     * @return An instance of an object representing the top-level element.
     * @throws ParseException
     *             if a parse error occurs.
     */
    public Object deserialize(final Reader reader) throws ParseException {
        final JsonHandler handler = new JsonHandler(reader);
        return handler.object();
    }

    /**
     * Deserialize a source given as String.
     * 
     * @param jsonString
     *            String for input.
     * @return An instance of an object representing the top-level element.
     * @throws ParseException
     *             if a parse error occurs.
     */
    public Object deserialize(final String jsonString) throws ParseException {
        final StringReader reader = new StringReader(jsonString);
        return this.deserialize(reader);
    }
}
