This project is obsolete. Please use [Jackson](https://github.com/FasterXML/jackson) instead. The object mapper there includes all the functionality which was in here.
